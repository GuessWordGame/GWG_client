package client.model;

import java.time.LocalDate;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;

public class Player {

    private final StringProperty alias;
    private final StringProperty ID;

    public Player(String ID, String alias) {
        this.alias = new SimpleStringProperty(alias);
        this.ID = new SimpleStringProperty(ID);
    }

    public String getAlias() {
        return alias.get();
    }

    public int getID() {
        return Integer.parseInt(ID.get());
    }

    public StringProperty idProperty() {
        return ID;
    }

    public StringProperty aliasProperty() {
        return alias;
    }
}