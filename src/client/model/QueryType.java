package client.model;

/**
 * Created by hamon on 5/9/17.
 */
public enum QueryType {
    ID,
    PLAYERS, REQUEST,CHOOSEWORD,GUESS,ACCEPTED, FINALGUESS, RESULT, END, DENIED;
}
