package client.view;

import client.MainApp;
import client.model.Player;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class PlayerOverviewController {
    @FXML
    private TableView<Player> playerTable;
    @FXML
    private TableColumn<Player, String> idColumn;
    @FXML
    private TableColumn<Player, String> aliasColumn;

    private MainApp mainApp;

    public PlayerOverviewController() {
    }

    @FXML
    private void initialize() {
        // Initialize the person table with the two columns.
        idColumn.setCellValueFactory(cellData -> cellData.getValue().idProperty());
        aliasColumn.setCellValueFactory(cellData -> cellData.getValue().aliasProperty());

    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
        playerTable.setItems(mainApp.getPlayersData());
    }

    @FXML
    private void handleRequest() {
        int selectedIndex = playerTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            Player tmp = playerTable.getItems().get(selectedIndex);
            mainApp.send("REQUEST "+tmp.getID());
        } else {
            // Nothing selected.
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Player Selected");
            alert.setContentText("Please select a player in the table.");
            alert.showAndWait();
        }
    }

    @FXML
    private void handleRefresh() {
        mainApp.send("PLAYERS");
    }
}