package client.view;

import client.MainApp;
import client.Match;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * Created by hamon on 5/9/17.
 */
public class GameChooseLayoutController {
    private MainApp mainApp;
    @FXML
    private TextField input;
    @FXML
    private Label label;
    @FXML
    private Label warning;
    @FXML
    private Button sendButton;
    private Match match;

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    public void sendWord() {
        String word = input.getText();
        for (int i = 0; i < word.length(); i++) {
            if(!Character.isLetter(word.charAt(i)))
            {
                warning.setVisible(true);
                return;
            }
        }
        if(word.length()<5){
            warning.setVisible(true);
            return;
        }
        warning.setText("Sent");
        warning.setVisible(true);
        mainApp.send("WORD "+match.getID()+" "+word);
        sendButton.setDisable(true);
        label.setText("Your Word: "+word);
    }

    public void setMatch(Match match) {
        this.match = match;
    }
}
