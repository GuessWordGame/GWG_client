package client.view;

import client.MainApp;
import client.Match;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 * Created by hamon on 5/21/17.
 */
public class GameOverLayoutController {
    @FXML
    private Label result;
    @FXML
    private Label scores;
    private MainApp mainApp;
    private Match match;

    @FXML
    public void close(){
        mainApp.closeMatch(this.match);
    }

    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public void setResult(int my, int op) {
        scores.setText("You: "+my+"        "+match.getOpponent()+": "+op);
        if(my>op)
        {
            result.setText("You Won!");
            result.setStyle("-fx-text-fill: #0b7c00");
        }
        else if(my<op)
        {
            result.setText("You Lost!");
            result.setStyle("-fx-text-fill: #7c1200");
        }
        else
        {
            result.setText("Draw!");
            result.setStyle("-fx-text-fill: dodgerblue");
        }
    }
}
