package client.view;

import client.MainApp;
import client.Match;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * Created by hamon on 5/12/17.
 */
public class GameGuessLayoutController {
    private MainApp mainApp;
    private Match match;
    @FXML
    private TextField input;
    @FXML
    private Label word;
    @FXML
    private Button guessLetterButton;
    @FXML
    private Button guessWordButton;
    @FXML
    private Label warning;
    @FXML
    private Label guesses;
    @FXML
    private Label moves;
    private int movesCounter = 0;

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public void setWord(String word) {
        this.word.setText(word);
        guessLetterButton.setDisable(false);
        guessWordButton.setDisable(false);
    }

    public void guessLetter() {
        String l = input.getText();
        if(l.length()>1 || l.length()==0 || !Character.isLetter(l.charAt(0)))
        {
            warning.setText("It should be one latin letter");
            warning.setVisible(true);
            return;
        }
        mainApp.send("GUESS "+match.getID()+' '+l);
        guesses.setText(guesses.getText()+"/"+l);
        movesCounter++;
//        moves.setText(movesCounter+" / "+word.getText().length());
        input.setText("");
        warning.setVisible(false);
        guessLetterButton.setDisable(true);
        guessWordButton.setDisable(true);
    }

    public void guessWord() {
        String l = input.getText();
        if(l.length()!=word.getText().length())
        {
            warning.setText("It should be a "+word.getText().length()+" letters word");
            warning.setVisible(true);
            return;
        }
        mainApp.send("FINALGUESS "+match.getID()+' '+l);
        warning.setVisible(false);
        guessWordButton.setDisable(true);
        guessLetterButton.setDisable(true);
    }

    public void finalGuess(String word) {
        guessLetterButton.setDisable(true);
        guessWordButton.setDisable(false);
        warning.setText("Final Move! Guess the word.");
        warning.setVisible(true);
        this.word.setText(word);
    }
}
