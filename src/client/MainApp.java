package client;

import client.model.Player;
import client.view.PlayerOverviewController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Optional;

public class MainApp extends Application {

    private Stage primaryStage;
    private TabPane rootLayout;
    private PrintWriter out;
    private HashMap<Integer,Match> matches;

    private ObservableList<Player> playersData = FXCollections.observableArrayList();
    private int id;
    private String alias = "user";
    private Socket socket;

    public MainApp(){
        matches = new HashMap<>();
    }

    public ObservableList<Player> getPlayersData() {
        return playersData;
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            socket = new Socket("localhost",1111);
            out = new PrintWriter(socket.getOutputStream());
            new SocketInputController(this,socket.getInputStream()).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Game");
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle("Login");
        dialog.setHeaderText("Welcome");
        dialog.setContentText("Please choose an alias:");
        Optional<String> result = dialog.showAndWait();
        result.ifPresent(alias -> {this.alias = alias;send("NEW "+alias);});
        initRootLayout();
        showPlayerOverview();
    }

    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
            rootLayout = (TabPane) loader.load();
            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
            primaryStage.setOnCloseRequest(event -> {
                Platform.exit();
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    System.exit(0);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the person overview inside the root layout.
     */
    public void showPlayerOverview() {
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/PlayerOverview.fxml"));
            AnchorPane personOverview = (AnchorPane) loader.load();
            // Add person overview as a tab into root layout.
            Tab tmp = new Tab("Home");
            tmp.setContent(personOverview);
            rootLayout.getTabs().add(tmp);
            // Give the controller access to the main app.
            PlayerOverviewController controller = loader.getController();
            controller.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }

    public void send(String message) {
        System.out.println("send: "+message);
        out.write(message+"\n");
        out.flush();
    }

    public void setId(int id) {
        this.id = id;
    }

    public void gameRequest(String alias, int gameID) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                        "Accept Game Request From '" + alias +"' ?" , ButtonType.YES, ButtonType.NO);
                alert.showAndWait();

                if (alert.getResult() == ButtonType.YES) {
                    matches.put(gameID, new Match(gameID,MainApp.this,alias));
                    send("ACCEPT "+gameID);
                }
                else
                    send("DENY "+gameID);
            }
        });
    }

    public Match getMatch(int ID) {
        return matches.get(ID);
    }

    public void addTab(Tab tab) {
        rootLayout.getTabs().add(tab);
    }

    public String getAlias() {
        return alias;
    }

    public void addMatch(Match match) {
        matches.put(match.getID(),match);
    }

    public void closeMatch(Match match) {
        rootLayout.getTabs().removeAll(match.getTab());
        matches.remove(match.getID());
    }
}