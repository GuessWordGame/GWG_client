package client;

import client.view.GameChooseLayoutController;
import client.view.GameGuessLayoutController;
import client.view.GameOverLayoutController;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

/**
 * Created by hamon on 5/11/17.
 */
public class Match {

    int ID;
    MainApp mainApp;
    String opponent;
    GameChooseLayoutController chooseController;
    Tab tab;
    boolean chooseMode;
    private GameGuessLayoutController guessController;

    public Match(int ID, MainApp mainApp, String opponent) {
        this.ID = ID;
        this.mainApp = mainApp;
        this.opponent = opponent;
    }

    public int getID() {
        return ID;
    }

    public void chooseWord() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainApp.class.getResource("view/GameChooseLayout.fxml"));
                AnchorPane game = null;
                try {
                    game = (AnchorPane) loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // Add person overview as a tab into root layout.
                if(tab==null) {
                    tab = new Tab("Game"+Match.this.ID+ " - vs " + opponent);
                    mainApp.addTab(tab);
                }
                tab.setContent(game);
                // Give the controller access to the main app and match object.
                chooseController = loader.getController();
                chooseController.setMainApp(mainApp);
                chooseController.setMatch(Match.this);
                chooseMode = true;
            }
        });
    }

    public void guess(String word) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if(chooseMode || tab==null){
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(MainApp.class.getResource("view/GameGuessLayout.fxml"));
                    AnchorPane game = null;
                    try {
                        game = (AnchorPane) loader.load();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // Add person overview as a tab into root layout.
                    if(tab==null) {
                        tab = new Tab("Game"+Match.this.ID+ " - vs " + opponent);
                        mainApp.addTab(tab);
                    }
                    tab.setContent(game);
                    //give controller access to the main app and the match object
                    guessController = loader.getController();
                    guessController.setMainApp(mainApp);
                    guessController.setMatch(Match.this);
                    chooseMode = false;
                }
                guessController.setWord(word);
            }
        });
    }

    public void finalGuess(String word) {
        Platform.runLater(()->guessController.finalGuess(word));
    }

    public void result(String res) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Alert alert = new Alert(Alert.AlertType.INFORMATION,opponent+ "msg", ButtonType.OK);
                alert.setTitle("Game"+Match.this.ID);
                if(chooseMode==true) {
                    if(res.compareTo("CORRECT")==0)
                        alert.setContentText(opponent+" guessed your word correctly.");
                    else
                        alert.setContentText(opponent+" couldn't guess your word.");
                }
                else {
                    if(res.compareTo("CORRECT")==0)
                        alert.setContentText("You guessed the word correctly.");
                    else
                        alert.setContentText("You couldn't the your word.");
                }
                alert.showAndWait();
            }
        });
    }

    public void gameOver(int myScore, int opScore) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainApp.class.getResource("view/GameOverLayout.fxml"));
                AnchorPane game = null;
                try {
                    game = (AnchorPane) loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if(tab==null) {
                    tab = new Tab("Game"+Match.this.ID+ " - vs " + opponent);
                    mainApp.addTab(tab);
                }
                tab.setContent(game);
                //give controller access to the main app and the match object
                GameOverLayoutController gameoverController = loader.getController();
                gameoverController.setMainApp(mainApp);
                gameoverController.setMatch(Match.this);
                gameoverController.setResult(myScore,opScore);
            }
        });
    }

    public String getOpponent() {
        return opponent;
    }

    public Tab getTab() {
        return tab;
    }
}
