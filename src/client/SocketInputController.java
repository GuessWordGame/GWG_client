package client;

import client.model.Player;
import client.model.QueryType;
import javafx.collections.ObservableList;

import java.io.InputStream;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * Created by hamon on 4/28/17.
 */
public class SocketInputController extends Thread{
    MainApp mainApp;
    Scanner in;
    public SocketInputController(MainApp mainApp, InputStream inputStream) {
        this.mainApp = mainApp;
        in = new Scanner(inputStream);
    }

    @Override
    public void run(){
        while(in.hasNextLine()) {
            handleQuery(in.nextLine());
        }
    }

    private void handleQuery(String q) {
        System.out.println("received: "+ q);
        StringTokenizer st = new StringTokenizer(q);
        QueryType type = QueryType.valueOf(st.nextToken());
        switch (type) {
            case ID:
                mainApp.setId(Integer.parseInt(st.nextToken()));
                break;
            case PLAYERS:
                updatePlayers(Integer.parseInt(st.nextToken()));
                break;
            case REQUEST:
                mainApp.gameRequest(st.nextToken(),Integer.parseInt(st.nextToken()));
                break;
            case CHOOSEWORD:
                mainApp.getMatch(Integer.parseInt(st.nextToken())).chooseWord();
                break;
            case ACCEPTED:
                mainApp.addMatch(new Match(Integer.parseInt(st.nextToken()),mainApp,st.nextToken()));
                break;
            case GUESS:
                mainApp.getMatch(Integer.parseInt(st.nextToken())).guess(st.nextToken());
                break;
            case FINALGUESS:
                mainApp.getMatch(Integer.parseInt(st.nextToken())).finalGuess(st.nextToken());
                break;
            case RESULT:
                mainApp.getMatch(Integer.parseInt(st.nextToken())).result(st.nextToken());
                break;
            case END:
                mainApp.getMatch(Integer.parseInt(st.nextToken())).gameOver(Integer.parseInt(st.nextToken()),
                                Integer.parseInt(st.nextToken()));
                break;
            case DENIED:
        }

    }

    private void updatePlayers(int n) {
        ObservableList<Player> p = mainApp.getPlayersData();
        p.clear();
        for (int i = 0; i < n; i++) {
            StringTokenizer st = new StringTokenizer(in.nextLine());
            p.add(new Player(st.nextToken(),st.nextToken()));
        }
    }
}
